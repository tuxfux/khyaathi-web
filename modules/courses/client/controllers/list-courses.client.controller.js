(function () {
  'use strict';

  angular
    .module('courses')
    .controller('CoursesListController', CoursesListController);

  CoursesListController.$inject = ['CoursesService', 'Authentication', '$scope', '$state','$http'];

  function CoursesListController(CoursesService, Authentication, $scope, $state,$http) {
    var vm = this;
    $scope.authentication = Authentication;

    vm.courses = CoursesService.query();
    //console.log(vm.courses);
    $scope.deleteCourse = function (deleteItem, index) {
      if (confirm('Are you sure you want to delete?')) {
        vm.courses.splice(index, 1);
        deleteItem.$remove($state.go('courses.list'));
      }
    };

    $scope.hashcourse = function (hashItem, index) {
      if(hashItem.hashIcon === true){
        hashItem.hashIcon = false;
      }else{
        hashItem.hashIcon = true;
      }
      $http.post('/api/courses/update/hashcode',hashItem).then(function success(req) {
      }).then(function error(err) {

      });
    };
  }
}());
