(function() {
  'use strict';

  angular
    .module('core')
    .controller('ScheduleController', ScheduleController);

  ScheduleController.$inject = ['$scope'];

  function ScheduleController($scope) {
    var vm = this;

    // Schedule controller logic
    // ...

    init();

    function init() {
    }
  }
})();
