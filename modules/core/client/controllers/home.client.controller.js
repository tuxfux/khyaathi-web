'use strict';

angular.module('core').controller('HomeController', ['$scope', 'Authentication','CoursesService',
  function ($scope, Authentication,CoursesService) {
    // This provides Authentication context.
    $scope.authentication = Authentication;
    var myIndex = 0;
    var vm = this;
    $scope.courses = CoursesService.query();
    console.log($scope.courses);
    carousel();

    init();

    function init() {
      $scope.courses = CoursesService.query();
    }

    var modal = '';
    if (getCookie('modalLoad') === 'no') {

    } else {
      setCookie('modalLoad', 'no', 1);
    }

    setTimeout(function () {
      if (getCookie('modalLoad') === 'no') {
        modal = document.getElementById('myModal');
        //console.log(modal);
        modal.style.display = 'block';
        setCookie('modalLoad', 'yes', 1);
      }
    }, 2000);

    window.onclick = function (event) {
      modal = document.getElementById('myModal');
      if (event.target === modal) {
        modal.style.display = 'none';
      }
    };

    function setCookie(name, value, days) {
      var expires = '';
      if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = '; expires=' + date.toUTCString();
      }
      document.cookie = name + '=' + (value || '') + expires + '; path=/';
    }

    $scope.closeModal = function () {
      //console.log('hello world');
      modal = document.getElementById('myModal');
      modal.style.display = 'none';
    };

    function getCookie(name) {
      var nameEQ = name + '=';
      var ca = document.cookie.split(';');
      for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length, c.length);
      }
      return null;
    }

    function eraseCookie(name) {
      document.cookie = name + '=; Max-Age=-99999999;';
    }

    function carousel() {
      var i;
      var x = document.getElementsByClassName('mySlides');
      for (i = 0; i < x.length; i++) {
        x[i].style.display = 'none';
      }
      myIndex++;
      if (myIndex > x.length) {
        myIndex = 1;
      }
      x[myIndex - 1].style.display = 'block';
      setTimeout(carousel, 2000); // Change image every 2 seconds
    }
  }
]);
