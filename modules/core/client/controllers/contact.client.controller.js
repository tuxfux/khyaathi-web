(function() {
  'use strict';

  angular
    .module('core')
    .controller('ContactController', ContactController);

  ContactController.$inject = ['$scope','$http','toastr'];

  function ContactController($scope,$http,toastr) {
    var vm = this;
    $scope.disableValue = false;
    //$scope.contactData = {};

    // Contact controller logic
    // ...
    var modal;

    init();

    function init() {
    }

    $scope.contactEmail = function(isValid){
      $scope.disableValue = true;
      $http.post('/contact/emailsend',$scope.contactData).success(function(res){
        //console.log('succes msg');
        //console.log(res);
        $scope.contactData = {};
        $scope.disableValue = false;
        toastr.info('Your information has been sent. We will contact you soon', 'Contact you soon.');
      }).error(function(err){
        //console.log('error msg');
        $scope.disableValue = true;
        toastr.error('Your information is not send. Please try later after sometime', '');
      });
    };
    $scope.closeModal = function () {
      //console.log('hello world');
      modal = document.getElementById('myModal');
      modal.style.display = 'none';
    };

    window.onclick = function (event) {
      modal = document.getElementById('myModal');
      if (event.target === modal) {
        modal.style.display = 'none';
      }
    };

    $scope.contactEmailModal = function(isValid){
      $scope.disableValue = true;
      $http.post('/contact/emailsend',$scope.contactData).success(function(res){
        //console.log('succes msg');
        //console.log(res);
        $scope.contactData = {};
        $scope.disableValue = false;
        toastr.info('Your information has been sent. We will contact you soon', 'Contact you soon.');
        modal = document.getElementById('myModal');
        modal.style.display = 'none';
      }).error(function(err){
        //console.log('error msg');
        $scope.disableValue = true;
        toastr.error('Your information is not send. Please try later after sometime', '');
      });
    };

  }
})();
