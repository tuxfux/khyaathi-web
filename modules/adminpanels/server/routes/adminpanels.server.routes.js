'use strict';

/**
 * Module dependencies
 */
var adminpanelsPolicy = require('../policies/adminpanels.server.policy'),
  adminpanels = require('../controllers/adminpanels.server.controller');

module.exports = function(app) {
  // Adminpanels Routes
  app.route('/api/adminpanels').all(adminpanelsPolicy.isAllowed)
    .get(adminpanels.list)
    .post(adminpanels.create);

  app.route('/api/adminpanels/:adminpanelId').all(adminpanelsPolicy.isAllowed)
    .get(adminpanels.read)
    .put(adminpanels.update)
    .delete(adminpanels.delete);

  app.route('/update/userDetails').post(adminpanels.updateUserDetails);

  // Finish by binding the Adminpanel middleware
  app.param('adminpanelId', adminpanels.adminpanelByID);
};
