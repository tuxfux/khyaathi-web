'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Adminpanel = mongoose.model('Adminpanel'),
  User = mongoose.model('User'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash');

/**
 * Create a Adminpanel
 */
exports.create = function(req, res) {
  var adminpanel = new Adminpanel(req.body);
  adminpanel.user = req.user;

  adminpanel.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(adminpanel);
    }
  });
};

/**
 * Show the current Adminpanel
 */
exports.read = function(req, res) {
  // convert mongoose document to JSON
  console.log(req);
  var adminpanel = req.adminpanel ? req.adminpanel.toJSON() : {};

  // Add a custom field to the Article, for determining if the current User is the "owner".
  // NOTE: This field is NOT persisted to the database, since it doesn't exist in the Article model.
  adminpanel.isCurrentUserOwner = req.user && adminpanel.user && adminpanel.user._id.toString() === req.user._id.toString();

  res.jsonp(adminpanel);
};

/**
 * Update a Adminpanel
 */
exports.update = function(req, res) {
  var adminpanel = req.adminpanel;

  adminpanel = _.extend(adminpanel, req.body);

  adminpanel.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(adminpanel);
    }
  });
};

/**
 * Delete an Adminpanel
 */
exports.delete = function(req, res) {
  var adminpanel = req.adminpanel;

  adminpanel.remove(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(adminpanel);
    }
  });
};

/**
 * List of Adminpanels
 */
exports.list = function(req, res) {
  User.find().sort('-created').populate('user', 'displayName').exec(function(err, adminpanels) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(adminpanels);
    }
  });
};

/**
 * Adminpanel middleware
 */
exports.adminpanelByID = function(req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Adminpanel is invalid'
    });
  }

  User.findById(id).populate('user', 'displayName').exec(function (err, adminpanel) {
    if (err) {
      return next(err);
    } else if (!adminpanel) {
      return res.status(404).send({
        message: 'No Adminpanel with that identifier has been found'
      });
    }
    req.adminpanel = adminpanel;
    next();
  });
};

/* update admin user */
exports.updateUserDetails = function(req,res){
  console.log(req.body);
  var userDetails = req.body;
  User.findOneAndUpdate({ _id:userDetails._id },{ $set:{ roles : userDetails.roles } },function(err){

    if(err)
    {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    }

    else
      res.jsonp();

  });
  res.jsonp();
};
