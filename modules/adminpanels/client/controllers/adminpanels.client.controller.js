(function () {
  'use strict';

  // Adminpanels controller
  angular
    .module('adminpanels')
    .controller('AdminpanelsController', AdminpanelsController);

  AdminpanelsController.$inject = ['$scope', '$state', '$window', 'Authentication', 'adminpanelResolve','$http'];

  function AdminpanelsController ($scope, $state, $window, Authentication, adminpanel,$http) {
    var vm = this;

    vm.authentication = Authentication;
    vm.adminpanel = adminpanel;
    vm.error = null;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;
    vm.updateUser = updateUser;

    console.log(vm.adminpanel);

    for(var i =0;i<vm.adminpanel.roles.length;i++){
      if(vm.adminpanel.roles[i]==='admin'){
        vm.adminpanel.roleadmin = true;
      }
      if(vm.adminpanel.roles[i]==='user'){
        vm.adminpanel.roleuser = true;
      }
    }

    // Remove existing Adminpanel
    function remove() {
      if ($window.confirm('Are you sure you want to delete?')) {
        vm.adminpanel.$remove($state.go('adminpanels.list'));
      }
    }

    // Save Adminpanel
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.adminpanelForm');
        return false;
      }

      // TODO: move create/update logic to service
      if (vm.adminpanel._id) {
        vm.adminpanel.$update(successCallback, errorCallback);
      } else {
        vm.adminpanel.$save(successCallback, errorCallback);
      }

      function successCallback(res) {
        $state.go('adminpanels.view', {
          adminpanelId: res._id
        });
      }

      function errorCallback(res) {
        vm.error = res.data.message;
      }
    }

    function updateUser(userDetails){
      console.log(userDetails);
      var dummyRoles = userDetails.roles;
      userDetails.roles = [];
      if(userDetails.roleadmin){
        userDetails.roles.push("admin");
      }
      if(userDetails.roleuser){
        userDetails.roles.push("user");
      }
      if(userDetails.roles.length === 0){
        userDetails.roles = dummyRoles;
      }
      console.log(userDetails);
      var userUpdateDetails = {};
      userUpdateDetails._id = userDetails._id;
      userUpdateDetails.roles = userDetails.roles;
      $http.post('/update/userDetails',userUpdateDetails).then(function success(res){
        console.log(res);
      },function error(res){
        console.log(res);
      });
    }
  }
}());
