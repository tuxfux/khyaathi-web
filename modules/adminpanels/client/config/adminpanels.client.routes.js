(function () {
  'use strict';

  angular
    .module('adminpanels')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('adminpanels', {
        abstract: true,
        url: '/adminpanels',
        template: '<ui-view/>'
      })
      .state('adminpanels.list', {
        url: '',
        templateUrl: 'modules/adminpanels/client/views/list-adminpanels.client.view.html',
        controller: 'AdminpanelsListController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Adminpanels List'
        }
      })
      .state('adminpanels.create', {
        url: '/create',
        templateUrl: 'modules/adminpanels/client/views/form-adminpanel.client.view.html',
        controller: 'AdminpanelsController',
        controllerAs: 'vm',
        resolve: {
          adminpanelResolve: newAdminpanel
        },
        data: {
          roles: ['user', 'admin'],
          pageTitle: 'Adminpanels Create'
        }
      })
      .state('adminpanels.edit', {
        url: '/:adminpanelId/edit',
        templateUrl: 'modules/adminpanels/client/views/form-adminpanel.client.view.html',
        controller: 'AdminpanelsController',
        controllerAs: 'vm',
        resolve: {
          adminpanelResolve: getAdminpanel
        },
        data: {
          roles: ['user', 'admin'],
          pageTitle: 'Edit Adminpanel {{ adminpanelResolve.name }}'
        }
      })
      .state('adminpanels.view', {
        url: '/:adminpanelId',
        templateUrl: 'modules/adminpanels/client/views/view-adminpanel.client.view.html',
        controller: 'AdminpanelsController',
        controllerAs: 'vm',
        resolve: {
          adminpanelResolve: getAdminpanel
        },
        data: {
          pageTitle: 'Adminpanel {{ adminpanelResolve.name }}'
        }
      });
  }

  getAdminpanel.$inject = ['$stateParams', 'AdminpanelsService'];

  function getAdminpanel($stateParams, AdminpanelsService) {
    return AdminpanelsService.get({
      adminpanelId: $stateParams.adminpanelId
    }).$promise;
  }

  newAdminpanel.$inject = ['AdminpanelsService'];

  function newAdminpanel(AdminpanelsService) {
    return new AdminpanelsService();
  }
}());
