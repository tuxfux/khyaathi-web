// (function () {
//   'use strict';

//   angular
//     .module('adminpanels')
//     .run(menuConfig);

//   menuConfig.$inject = ['menuService'];

//   function menuConfig(menuService) {
//     // Set top bar menu items
//     menuService.addMenuItem('topbar', {
//       title: 'Adminpanels',
//       state: 'adminpanels',
//       type: 'dropdown',
//       roles: ['*']
//     });

//     // Add the dropdown list item
//     menuService.addSubMenuItem('topbar', 'adminpanels', {
//       title: 'List Adminpanels',
//       state: 'adminpanels.list'
//     });

//     // Add the dropdown create item
//     menuService.addSubMenuItem('topbar', 'adminpanels', {
//       title: 'Create Adminpanel',
//       state: 'adminpanels.create',
//       roles: ['user']
//     });
//   }
// }());
