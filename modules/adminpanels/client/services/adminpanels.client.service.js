// Adminpanels service used to communicate Adminpanels REST endpoints
(function () {
  'use strict';

  angular
    .module('adminpanels')
    .factory('AdminpanelsService', AdminpanelsService);

  AdminpanelsService.$inject = ['$resource'];

  function AdminpanelsService($resource) {
    return $resource('api/adminpanels/:adminpanelId', {
      adminpanelId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
}());
