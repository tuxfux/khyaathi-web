(function () {
  'use strict';

  describe('Adminpanels Controller Tests', function () {
    // Initialize global variables
    var AdminpanelsController,
      $scope,
      $httpBackend,
      $state,
      Authentication,
      AdminpanelsService,
      mockAdminpanel;

    // The $resource service augments the response object with methods for updating and deleting the resource.
    // If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
    // the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
    // When the toEqualData matcher compares two objects, it takes only object properties into
    // account and ignores methods.
    beforeEach(function () {
      jasmine.addMatchers({
        toEqualData: function (util, customEqualityTesters) {
          return {
            compare: function (actual, expected) {
              return {
                pass: angular.equals(actual, expected)
              };
            }
          };
        }
      });
    });

    // Then we can start by loading the main application module
    beforeEach(module(ApplicationConfiguration.applicationModuleName));

    // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
    // This allows us to inject a service but then attach it to a variable
    // with the same name as the service.
    beforeEach(inject(function ($controller, $rootScope, _$state_, _$httpBackend_, _Authentication_, _AdminpanelsService_) {
      // Set a new global scope
      $scope = $rootScope.$new();

      // Point global variables to injected services
      $httpBackend = _$httpBackend_;
      $state = _$state_;
      Authentication = _Authentication_;
      AdminpanelsService = _AdminpanelsService_;

      // create mock Adminpanel
      mockAdminpanel = new AdminpanelsService({
        _id: '525a8422f6d0f87f0e407a33',
        name: 'Adminpanel Name'
      });

      // Mock logged in user
      Authentication.user = {
        roles: ['user']
      };

      // Initialize the Adminpanels controller.
      AdminpanelsController = $controller('AdminpanelsController as vm', {
        $scope: $scope,
        adminpanelResolve: {}
      });

      // Spy on state go
      spyOn($state, 'go');
    }));

    describe('vm.save() as create', function () {
      var sampleAdminpanelPostData;

      beforeEach(function () {
        // Create a sample Adminpanel object
        sampleAdminpanelPostData = new AdminpanelsService({
          name: 'Adminpanel Name'
        });

        $scope.vm.adminpanel = sampleAdminpanelPostData;
      });

      it('should send a POST request with the form input values and then locate to new object URL', inject(function (AdminpanelsService) {
        // Set POST response
        $httpBackend.expectPOST('api/adminpanels', sampleAdminpanelPostData).respond(mockAdminpanel);

        // Run controller functionality
        $scope.vm.save(true);
        $httpBackend.flush();

        // Test URL redirection after the Adminpanel was created
        expect($state.go).toHaveBeenCalledWith('adminpanels.view', {
          adminpanelId: mockAdminpanel._id
        });
      }));

      it('should set $scope.vm.error if error', function () {
        var errorMessage = 'this is an error message';
        $httpBackend.expectPOST('api/adminpanels', sampleAdminpanelPostData).respond(400, {
          message: errorMessage
        });

        $scope.vm.save(true);
        $httpBackend.flush();

        expect($scope.vm.error).toBe(errorMessage);
      });
    });

    describe('vm.save() as update', function () {
      beforeEach(function () {
        // Mock Adminpanel in $scope
        $scope.vm.adminpanel = mockAdminpanel;
      });

      it('should update a valid Adminpanel', inject(function (AdminpanelsService) {
        // Set PUT response
        $httpBackend.expectPUT(/api\/adminpanels\/([0-9a-fA-F]{24})$/).respond();

        // Run controller functionality
        $scope.vm.save(true);
        $httpBackend.flush();

        // Test URL location to new object
        expect($state.go).toHaveBeenCalledWith('adminpanels.view', {
          adminpanelId: mockAdminpanel._id
        });
      }));

      it('should set $scope.vm.error if error', inject(function (AdminpanelsService) {
        var errorMessage = 'error';
        $httpBackend.expectPUT(/api\/adminpanels\/([0-9a-fA-F]{24})$/).respond(400, {
          message: errorMessage
        });

        $scope.vm.save(true);
        $httpBackend.flush();

        expect($scope.vm.error).toBe(errorMessage);
      }));
    });

    describe('vm.remove()', function () {
      beforeEach(function () {
        // Setup Adminpanels
        $scope.vm.adminpanel = mockAdminpanel;
      });

      it('should delete the Adminpanel and redirect to Adminpanels', function () {
        // Return true on confirm message
        spyOn(window, 'confirm').and.returnValue(true);

        $httpBackend.expectDELETE(/api\/adminpanels\/([0-9a-fA-F]{24})$/).respond(204);

        $scope.vm.remove();
        $httpBackend.flush();

        expect($state.go).toHaveBeenCalledWith('adminpanels.list');
      });

      it('should should not delete the Adminpanel and not redirect', function () {
        // Return false on confirm message
        spyOn(window, 'confirm').and.returnValue(false);

        $scope.vm.remove();

        expect($state.go).not.toHaveBeenCalled();
      });
    });
  });
}());
