(function () {
  'use strict';

  describe('Adminpanels Route Tests', function () {
    // Initialize global variables
    var $scope,
      AdminpanelsService;

    // We can start by loading the main application module
    beforeEach(module(ApplicationConfiguration.applicationModuleName));

    // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
    // This allows us to inject a service but then attach it to a variable
    // with the same name as the service.
    beforeEach(inject(function ($rootScope, _AdminpanelsService_) {
      // Set a new global scope
      $scope = $rootScope.$new();
      AdminpanelsService = _AdminpanelsService_;
    }));

    describe('Route Config', function () {
      describe('Main Route', function () {
        var mainstate;
        beforeEach(inject(function ($state) {
          mainstate = $state.get('adminpanels');
        }));

        it('Should have the correct URL', function () {
          expect(mainstate.url).toEqual('/adminpanels');
        });

        it('Should be abstract', function () {
          expect(mainstate.abstract).toBe(true);
        });

        it('Should have template', function () {
          expect(mainstate.template).toBe('<ui-view/>');
        });
      });

      describe('View Route', function () {
        var viewstate,
          AdminpanelsController,
          mockAdminpanel;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          viewstate = $state.get('adminpanels.view');
          $templateCache.put('modules/adminpanels/client/views/view-adminpanel.client.view.html', '');

          // create mock Adminpanel
          mockAdminpanel = new AdminpanelsService({
            _id: '525a8422f6d0f87f0e407a33',
            name: 'Adminpanel Name'
          });

          // Initialize Controller
          AdminpanelsController = $controller('AdminpanelsController as vm', {
            $scope: $scope,
            adminpanelResolve: mockAdminpanel
          });
        }));

        it('Should have the correct URL', function () {
          expect(viewstate.url).toEqual('/:adminpanelId');
        });

        it('Should have a resolve function', function () {
          expect(typeof viewstate.resolve).toEqual('object');
          expect(typeof viewstate.resolve.adminpanelResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(viewstate, {
            adminpanelId: 1
          })).toEqual('/adminpanels/1');
        }));

        it('should attach an Adminpanel to the controller scope', function () {
          expect($scope.vm.adminpanel._id).toBe(mockAdminpanel._id);
        });

        it('Should not be abstract', function () {
          expect(viewstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(viewstate.templateUrl).toBe('modules/adminpanels/client/views/view-adminpanel.client.view.html');
        });
      });

      describe('Create Route', function () {
        var createstate,
          AdminpanelsController,
          mockAdminpanel;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          createstate = $state.get('adminpanels.create');
          $templateCache.put('modules/adminpanels/client/views/form-adminpanel.client.view.html', '');

          // create mock Adminpanel
          mockAdminpanel = new AdminpanelsService();

          // Initialize Controller
          AdminpanelsController = $controller('AdminpanelsController as vm', {
            $scope: $scope,
            adminpanelResolve: mockAdminpanel
          });
        }));

        it('Should have the correct URL', function () {
          expect(createstate.url).toEqual('/create');
        });

        it('Should have a resolve function', function () {
          expect(typeof createstate.resolve).toEqual('object');
          expect(typeof createstate.resolve.adminpanelResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(createstate)).toEqual('/adminpanels/create');
        }));

        it('should attach an Adminpanel to the controller scope', function () {
          expect($scope.vm.adminpanel._id).toBe(mockAdminpanel._id);
          expect($scope.vm.adminpanel._id).toBe(undefined);
        });

        it('Should not be abstract', function () {
          expect(createstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(createstate.templateUrl).toBe('modules/adminpanels/client/views/form-adminpanel.client.view.html');
        });
      });

      describe('Edit Route', function () {
        var editstate,
          AdminpanelsController,
          mockAdminpanel;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          editstate = $state.get('adminpanels.edit');
          $templateCache.put('modules/adminpanels/client/views/form-adminpanel.client.view.html', '');

          // create mock Adminpanel
          mockAdminpanel = new AdminpanelsService({
            _id: '525a8422f6d0f87f0e407a33',
            name: 'Adminpanel Name'
          });

          // Initialize Controller
          AdminpanelsController = $controller('AdminpanelsController as vm', {
            $scope: $scope,
            adminpanelResolve: mockAdminpanel
          });
        }));

        it('Should have the correct URL', function () {
          expect(editstate.url).toEqual('/:adminpanelId/edit');
        });

        it('Should have a resolve function', function () {
          expect(typeof editstate.resolve).toEqual('object');
          expect(typeof editstate.resolve.adminpanelResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(editstate, {
            adminpanelId: 1
          })).toEqual('/adminpanels/1/edit');
        }));

        it('should attach an Adminpanel to the controller scope', function () {
          expect($scope.vm.adminpanel._id).toBe(mockAdminpanel._id);
        });

        it('Should not be abstract', function () {
          expect(editstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(editstate.templateUrl).toBe('modules/adminpanels/client/views/form-adminpanel.client.view.html');
        });

        xit('Should go to unauthorized route', function () {

        });
      });

    });
  });
}());
