(function () {
  'use strict';

  describe('Adminpanels List Controller Tests', function () {
    // Initialize global variables
    var AdminpanelsListController,
      $scope,
      $httpBackend,
      $state,
      Authentication,
      AdminpanelsService,
      mockAdminpanel;

    // The $resource service augments the response object with methods for updating and deleting the resource.
    // If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
    // the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
    // When the toEqualData matcher compares two objects, it takes only object properties into
    // account and ignores methods.
    beforeEach(function () {
      jasmine.addMatchers({
        toEqualData: function (util, customEqualityTesters) {
          return {
            compare: function (actual, expected) {
              return {
                pass: angular.equals(actual, expected)
              };
            }
          };
        }
      });
    });

    // Then we can start by loading the main application module
    beforeEach(module(ApplicationConfiguration.applicationModuleName));

    // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
    // This allows us to inject a service but then attach it to a variable
    // with the same name as the service.
    beforeEach(inject(function ($controller, $rootScope, _$state_, _$httpBackend_, _Authentication_, _AdminpanelsService_) {
      // Set a new global scope
      $scope = $rootScope.$new();

      // Point global variables to injected services
      $httpBackend = _$httpBackend_;
      $state = _$state_;
      Authentication = _Authentication_;
      AdminpanelsService = _AdminpanelsService_;

      // create mock article
      mockAdminpanel = new AdminpanelsService({
        _id: '525a8422f6d0f87f0e407a33',
        name: 'Adminpanel Name'
      });

      // Mock logged in user
      Authentication.user = {
        roles: ['user']
      };

      // Initialize the Adminpanels List controller.
      AdminpanelsListController = $controller('AdminpanelsListController as vm', {
        $scope: $scope
      });

      // Spy on state go
      spyOn($state, 'go');
    }));

    describe('Instantiate', function () {
      var mockAdminpanelList;

      beforeEach(function () {
        mockAdminpanelList = [mockAdminpanel, mockAdminpanel];
      });

      it('should send a GET request and return all Adminpanels', inject(function (AdminpanelsService) {
        // Set POST response
        $httpBackend.expectGET('api/adminpanels').respond(mockAdminpanelList);


        $httpBackend.flush();

        // Test form inputs are reset
        expect($scope.vm.adminpanels.length).toEqual(2);
        expect($scope.vm.adminpanels[0]).toEqual(mockAdminpanel);
        expect($scope.vm.adminpanels[1]).toEqual(mockAdminpanel);

      }));
    });
  });
}());
