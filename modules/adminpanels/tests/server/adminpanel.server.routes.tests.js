'use strict';

var should = require('should'),
  request = require('supertest'),
  path = require('path'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  Adminpanel = mongoose.model('Adminpanel'),
  express = require(path.resolve('./config/lib/express'));

/**
 * Globals
 */
var app,
  agent,
  credentials,
  user,
  adminpanel;

/**
 * Adminpanel routes tests
 */
describe('Adminpanel CRUD tests', function () {

  before(function (done) {
    // Get application
    app = express.init(mongoose);
    agent = request.agent(app);

    done();
  });

  beforeEach(function (done) {
    // Create user credentials
    credentials = {
      username: 'username',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create a new user
    user = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'test@test.com',
      username: credentials.username,
      password: credentials.password,
      provider: 'local'
    });

    // Save a user to the test db and create new Adminpanel
    user.save(function () {
      adminpanel = {
        name: 'Adminpanel name'
      };

      done();
    });
  });

  it('should be able to save a Adminpanel if logged in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Adminpanel
        agent.post('/api/adminpanels')
          .send(adminpanel)
          .expect(200)
          .end(function (adminpanelSaveErr, adminpanelSaveRes) {
            // Handle Adminpanel save error
            if (adminpanelSaveErr) {
              return done(adminpanelSaveErr);
            }

            // Get a list of Adminpanels
            agent.get('/api/adminpanels')
              .end(function (adminpanelsGetErr, adminpanelsGetRes) {
                // Handle Adminpanels save error
                if (adminpanelsGetErr) {
                  return done(adminpanelsGetErr);
                }

                // Get Adminpanels list
                var adminpanels = adminpanelsGetRes.body;

                // Set assertions
                (adminpanels[0].user._id).should.equal(userId);
                (adminpanels[0].name).should.match('Adminpanel name');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to save an Adminpanel if not logged in', function (done) {
    agent.post('/api/adminpanels')
      .send(adminpanel)
      .expect(403)
      .end(function (adminpanelSaveErr, adminpanelSaveRes) {
        // Call the assertion callback
        done(adminpanelSaveErr);
      });
  });

  it('should not be able to save an Adminpanel if no name is provided', function (done) {
    // Invalidate name field
    adminpanel.name = '';

    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Adminpanel
        agent.post('/api/adminpanels')
          .send(adminpanel)
          .expect(400)
          .end(function (adminpanelSaveErr, adminpanelSaveRes) {
            // Set message assertion
            (adminpanelSaveRes.body.message).should.match('Please fill Adminpanel name');

            // Handle Adminpanel save error
            done(adminpanelSaveErr);
          });
      });
  });

  it('should be able to update an Adminpanel if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Adminpanel
        agent.post('/api/adminpanels')
          .send(adminpanel)
          .expect(200)
          .end(function (adminpanelSaveErr, adminpanelSaveRes) {
            // Handle Adminpanel save error
            if (adminpanelSaveErr) {
              return done(adminpanelSaveErr);
            }

            // Update Adminpanel name
            adminpanel.name = 'WHY YOU GOTTA BE SO MEAN?';

            // Update an existing Adminpanel
            agent.put('/api/adminpanels/' + adminpanelSaveRes.body._id)
              .send(adminpanel)
              .expect(200)
              .end(function (adminpanelUpdateErr, adminpanelUpdateRes) {
                // Handle Adminpanel update error
                if (adminpanelUpdateErr) {
                  return done(adminpanelUpdateErr);
                }

                // Set assertions
                (adminpanelUpdateRes.body._id).should.equal(adminpanelSaveRes.body._id);
                (adminpanelUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should be able to get a list of Adminpanels if not signed in', function (done) {
    // Create new Adminpanel model instance
    var adminpanelObj = new Adminpanel(adminpanel);

    // Save the adminpanel
    adminpanelObj.save(function () {
      // Request Adminpanels
      request(app).get('/api/adminpanels')
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Array).and.have.lengthOf(1);

          // Call the assertion callback
          done();
        });

    });
  });

  it('should be able to get a single Adminpanel if not signed in', function (done) {
    // Create new Adminpanel model instance
    var adminpanelObj = new Adminpanel(adminpanel);

    // Save the Adminpanel
    adminpanelObj.save(function () {
      request(app).get('/api/adminpanels/' + adminpanelObj._id)
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Object).and.have.property('name', adminpanel.name);

          // Call the assertion callback
          done();
        });
    });
  });

  it('should return proper error for single Adminpanel with an invalid Id, if not signed in', function (done) {
    // test is not a valid mongoose Id
    request(app).get('/api/adminpanels/test')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'Adminpanel is invalid');

        // Call the assertion callback
        done();
      });
  });

  it('should return proper error for single Adminpanel which doesnt exist, if not signed in', function (done) {
    // This is a valid mongoose Id but a non-existent Adminpanel
    request(app).get('/api/adminpanels/559e9cd815f80b4c256a8f41')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'No Adminpanel with that identifier has been found');

        // Call the assertion callback
        done();
      });
  });

  it('should be able to delete an Adminpanel if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Adminpanel
        agent.post('/api/adminpanels')
          .send(adminpanel)
          .expect(200)
          .end(function (adminpanelSaveErr, adminpanelSaveRes) {
            // Handle Adminpanel save error
            if (adminpanelSaveErr) {
              return done(adminpanelSaveErr);
            }

            // Delete an existing Adminpanel
            agent.delete('/api/adminpanels/' + adminpanelSaveRes.body._id)
              .send(adminpanel)
              .expect(200)
              .end(function (adminpanelDeleteErr, adminpanelDeleteRes) {
                // Handle adminpanel error error
                if (adminpanelDeleteErr) {
                  return done(adminpanelDeleteErr);
                }

                // Set assertions
                (adminpanelDeleteRes.body._id).should.equal(adminpanelSaveRes.body._id);

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to delete an Adminpanel if not signed in', function (done) {
    // Set Adminpanel user
    adminpanel.user = user;

    // Create new Adminpanel model instance
    var adminpanelObj = new Adminpanel(adminpanel);

    // Save the Adminpanel
    adminpanelObj.save(function () {
      // Try deleting Adminpanel
      request(app).delete('/api/adminpanels/' + adminpanelObj._id)
        .expect(403)
        .end(function (adminpanelDeleteErr, adminpanelDeleteRes) {
          // Set message assertion
          (adminpanelDeleteRes.body.message).should.match('User is not authorized');

          // Handle Adminpanel error error
          done(adminpanelDeleteErr);
        });

    });
  });

  it('should be able to get a single Adminpanel that has an orphaned user reference', function (done) {
    // Create orphan user creds
    var _creds = {
      username: 'orphan',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create orphan user
    var _orphan = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'orphan@test.com',
      username: _creds.username,
      password: _creds.password,
      provider: 'local'
    });

    _orphan.save(function (err, orphan) {
      // Handle save error
      if (err) {
        return done(err);
      }

      agent.post('/api/auth/signin')
        .send(_creds)
        .expect(200)
        .end(function (signinErr, signinRes) {
          // Handle signin error
          if (signinErr) {
            return done(signinErr);
          }

          // Get the userId
          var orphanId = orphan._id;

          // Save a new Adminpanel
          agent.post('/api/adminpanels')
            .send(adminpanel)
            .expect(200)
            .end(function (adminpanelSaveErr, adminpanelSaveRes) {
              // Handle Adminpanel save error
              if (adminpanelSaveErr) {
                return done(adminpanelSaveErr);
              }

              // Set assertions on new Adminpanel
              (adminpanelSaveRes.body.name).should.equal(adminpanel.name);
              should.exist(adminpanelSaveRes.body.user);
              should.equal(adminpanelSaveRes.body.user._id, orphanId);

              // force the Adminpanel to have an orphaned user reference
              orphan.remove(function () {
                // now signin with valid user
                agent.post('/api/auth/signin')
                  .send(credentials)
                  .expect(200)
                  .end(function (err, res) {
                    // Handle signin error
                    if (err) {
                      return done(err);
                    }

                    // Get the Adminpanel
                    agent.get('/api/adminpanels/' + adminpanelSaveRes.body._id)
                      .expect(200)
                      .end(function (adminpanelInfoErr, adminpanelInfoRes) {
                        // Handle Adminpanel error
                        if (adminpanelInfoErr) {
                          return done(adminpanelInfoErr);
                        }

                        // Set assertions
                        (adminpanelInfoRes.body._id).should.equal(adminpanelSaveRes.body._id);
                        (adminpanelInfoRes.body.name).should.equal(adminpanel.name);
                        should.equal(adminpanelInfoRes.body.user, undefined);

                        // Call the assertion callback
                        done();
                      });
                  });
              });
            });
        });
    });
  });

  afterEach(function (done) {
    User.remove().exec(function () {
      Adminpanel.remove().exec(done);
    });
  });
});
